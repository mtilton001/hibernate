/**
 *
 * Java Main
 *
 * Main class calls either Add_Employee or Get_Employee class
 *
 * @author Matthew Tilton
 *
 *
 * Hibernate User Guide:
 * https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html
 *
 */

public class Main {
    public static void main(String[] args) {

       //Add_Employee.main(null);

       Get_Employee.main(null);

    }
}