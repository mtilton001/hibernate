import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * Java Add_Employee
 *
 * Add_Employee class creates new employee entry in employee table
 *
 * @author Matthew Tilton
 *
 *
 * Hibernate User Guide:
 * https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html
 *
 */

public class Add_Employee {
    public static void main(String[] args) {
        try {
            //create or initialize ManagerFactory using persistence.xml unit name
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            Employee employee = new Employee("Jordan", "Michael", "Athletics", "Shooting Guard", "5555555555", "mj@bulls.com");

            entityManager.getTransaction().begin();
            entityManager.persist(employee);
            entityManager.getTransaction().commit();

            entityManagerFactory.close();
        }
        catch (Exception e) {
            System.out.println("The system has failed to add an employee.");
        }
    }

}