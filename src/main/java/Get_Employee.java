import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 *
 * Java Get_Employee
 *
 * Get_Employee class queries employee table for all current employees
 *
 * @author Matthew Tilton
 *
 *
 * Hibernate User Guide:
 * https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html
 *
 */

public class Get_Employee {
    public static void main(String[] args) {
        try {
            //create or initialize ManagerFactory using persistence.xml unit name
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");
            EntityManager entityManager = entityManagerFactory.createEntityManager();

            List employees = entityManager.createNativeQuery(
                    "select * from employee", Employee.class)
                    .getResultList();
            for (Object o : employees) {
                Employee employee = (Employee) o;
                System.out.println(employee.getPid() + ": " +
                        employee.getLastName() + ", " +
                        employee.getFirstName() + " - " +
                        employee.getDepartment() + ": " +
                        employee.getJob() + " - " +
                        employee.getWorkPhone() + " - " +
                        employee.getEmail() + "\n");
            }

            entityManagerFactory.close();
        }
        catch (Exception e){
            System.out.println("The system has failed to provide the Employee Table.");
        }
    }
}
