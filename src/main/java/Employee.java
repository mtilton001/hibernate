import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import java.io.Serializable;

/**
 *
 * Java Employee
 *
 * Employee class creating database table mappings
 *
 * @author Matthew Tilton
 *
 */

@Entity
//assigns the table(s) to look at
@Table(name = "employee")

public class Employee implements Serializable {

    //designate primary key with @Id
    @Id
    //map pid column from table to pid variable with @Column
    @Column(name = "pid")
    private int pid;
    //Getter and Setter for pid
    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    //map lastName column from table to lastName variable with @Column
    @Column(name = "lastName")
    private String lastName;
    //Getter and Setter for lastName
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    //map firstName column from table to firstName variable with @Column
    @Column(name = "firstName")
    private String firstName;
    //Getter and Setter for firstName
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    //map department column from table to department variable with @Column
    @Column(name = "department")
    private String department;
    //Getter and Setter for department
    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    //map job column from table to job variable with @Column
    @Column(name = "job")
    private String job;
    //Getter and Setter for job
    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    //map workPhone column from table to workPhone variable with @Column
    @Column(name = "workPhone")
    private String workPhone;
    //Getter and Setter for workPhone
    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    //map email column from table to email variable with @Column
    @Column(name = "email")
    private String email;
    //Getter and Setter for email
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    //constructors
    public Employee() {
    }

    public Employee(String lastName, String firstName, String department, String job, String workPhone, String email) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.department = department;
        this.job = job;
        this.workPhone = workPhone;
        this.email = email;
    }
}
